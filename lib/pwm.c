#include "pwm.h"

#include <msp430.h>
#include <stdint.h>

const uint16_t pwmLimit = PWM_PERIOD*MAX_DUTY;

void initPWM(void){
    P1DIR |= 0x04;                     // P1.2 = output
    P1SEL |= 0x04;                     // P1.2 = TA1 output
    TACCR0 = PWM_PERIOD;               // PWM Period
    TACCTL1 = OUTMOD_7;                // TACCR1 reset/set
    TACCR1 = 10;                       // TACCR1 PWM Duty Cycle
    TACTL = TASSEL_2 + MC_1;           // SMCLK, upmode
}

void setPWM(uint16_t value){
    // protect from high dutycycles
    // Limited at 90% duty
    if(value > pwmLimit) value = pwmLimit;
    TACCR1 = value;
}
