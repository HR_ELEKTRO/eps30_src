/**
 *  @file i2c.h
 *  @brief Dit headerbestand bevat alle i2c prototypes
 *
 *  @author VersD
 *  @date   4 mei 2017
 */

#ifndef _I2C_H
#define _I2C_H
/**
 * \defgroup I2C
 * @brief I2C functionaliteiten
 *
 * De Universal Serial Communication Interface (USCI) module kan draaien in 3 modi:
 * I2C, SPI en UART. Met deze functies kun je de module in I2C modus gebruiken.
 *
 * De volgende stappen moeten worden ondernomen om i2c te kunnen gebruiken
 *
 * + Intialiseer de module @see i2cInitialize
 * + Stel de klokfrequentie in @see i2cSetClock
 * + Stel het I2C adres in @see i2cSetAddress
 * + Stuur data op @see i2cSendBytes
 *
 * @{
 *
 */
#include "clock.h"
#include <stdint.h>

//!De twee i2c modi
typedef enum {MASTER,SLAVE} i2cMode;

//! I2C bus controle commando's
#define I2C_RESTART     1<<8    /* repeated start */
#define I2C_READ        2<<8    /* read a byte */

//public functions
/*!
 * @brief Stel de USCI module in voor I2C gebruik (i.p.v. SPI)
 *
 * \param mode Stel de i2c module in als SLAVE of MASTER. @see i2cSetMode()
 */
void i2cInitialize(i2cMode mode);

/*!
 * @brief Stel hiermee in of de module MASTER of SLAVE is op de i2c bus.
 *
 * @code
 * i2cSetMode(MASTER)
 * @endcode
 *
 * \param mode Stel de i2c module in als SLAVE of MASTER.
 * @todo Implementeer slave modus
 */
void i2cSetMode(i2cMode mode);

/*!
 * @brief Stel het adres in van deze i2c module
 *
 * Elke i2c module heeft een uniek adres op bus.
 *
 * \param address Het 7 bits adres zonder read/write bit, volledig naar rechts geschoven. Dus bits 0-6 bevatten het adres.
 */
void i2cSetAddress(uint8_t address);

/*!
 * @brief Zet een databyte in de transmit-buffer.
 *
 * Deze functie roept geen start of stop conditie aan. Het is dus te gebruiken wanneer
 * data verstuurt moet worden die run-time gegenereerd wordt. Zorg er dan wel voor dat
 * de communicatie wordt gestart en gestopt @see i2cStart @see i2cStop
 *
 * \param byte De 8 bits data om te laten versturen.
 */
void i2cSendByte(const uint8_t byte);

/*!
 * @brief Start verbinding en stuur data
 *
 * Met deze functie kun je meerdere databytes versturen. De functie zet de verbinding
 * op stand door een startconditie te versturen, stuurt vervolgens alle meegegeven
 * data en eindigt met een stopconditie.
 *
 * @code
 * uint8_t data = {0x0A, 0xAC, 0xFF, 0x12};
 * i2cSendBytes(4, data);
 * @endcode
 *
 * \param length Het aantal bytes wat wordt verstuurd
 * \param bytes Een array met de te versturen bytes
 */
void i2cSendBytes(uint16_t length, const uint8_t bytes[]);

/*!
 * @brief Start verbinding en ontvang data
 *
 * Met deze functie kun je een enkele data byte ontvangen.
 *
 * @code
 * uint8_t data = i2cReadByte();
 * @endcode
 *
 */
uint8_t i2cReadByte(void);

/*!
 * @brief Start verbinding en ontvang data
 *
 * Met deze functie kun je meerdere data bytes ontvangen.
 *
 * @code
 * uint8_t data[4];
 * i2cReadBytes(4, data);
 * @endcode
 *
 * \param length Het aantal bytes wat wordt verstuurd
 * \param data Een array met de te versturen bytes
 *
 */
void i2cReadBytes(uint16_t length, uint8_t data[]);

/*!
 * @brief Eigen combinatie tussen verzenden en ontvangen
 *
 * Met deze functie kun je meerdere data bytes verzenden en ontvangen.
 * Dit kan handig zijn om bijvoorbeeld een bepaald register van een i2c slaaf uit te lezen.
 * Het adres van de slaaf moet al van tevoren ingesteld zijn.
 * De volgende commando bytes zijn mogelijk in de sequence array
 * *I2C_READ
 *
 * @code
 * uint8_t data[2];
 * uint8_t sequence = {0x5B, I2C_READ, I2C_READ};
 * i2cSendSequence(3, sequence, data);
 * @endcode
 *
 * \param length Het aantal bytes waaruit de seq. bestaat
 * \param sequence een array van bytes om te schrijven of speciale i2c commando's
 * \param data Een array waar de ontvangen bytes worden opgeslagen
 *
 */
// De volgende functie voert een sequentie van read en write operaties uit.
//
// -- I2C_READ
// Elke andere byte wordt naar de bus geschreven

void i2cSendSequence(uint16_t length, const uint16_t sequence[], uint8_t data[]);


/*!
 * @brief Stel de i2c klok in (wanneer in MASTER modus)
 *
 * In Master modus genereert de microcontroller de i2c-klok zelf.
 * Stel met deze functie in van welke klokbron de module moet draaien
 * en met welke waarde deze klok nog gedeeld moet worden. De uitkomst van
 * deze deling is de uiteindelijk gebruikte klokfrequentie.
 *
 * \param source De klokbron waarop de module zal draaien.
 * \param divider Deel de klokfrequentie door een 16 bits integer waarde.
 */
void i2cSetClock(clockSources source, uint16_t divider);

/*!
 * @brief Stuur een startconditie
 */
void i2cStart();

/*!
 * @brief Stuur een stopconditie en wacht op ACK
 */
void i2cStop();

/*!
 * @brief Stuur een stopconditie
 */
void i2cStopPrepare();

/*!
 * @brief Herstart de bus met een nieuwe start conditie
 */
void i2cRestart();

/**
 * @}
 */
#endif
