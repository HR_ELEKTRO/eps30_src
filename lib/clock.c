/*
 * clocks.c
 *
 *  Created on: 4 mei 2017
 *      Author: VersD
 */
#include "clock.h"

static volatile uint8_t wdcnt = 250;

void clkConfigureSMCLK(Oscillator bron, uint8_t deler)
{
    //standaard DCOCLK
    uint8_t sel=0;

    if(bron == LFXT1 || bron == VLO)
    {
        sel=1;
    }

    BCSCTL2 &= 0x0F;
    BCSCTL2 |= (sel<<6 | clkGetDivision(deler)<<4);
}

void clkConfigureACLK(Oscillator bron, uint8_t deler)
{
    if(bron == LFXT1)
    {
        clkStartOscillator(LFXT1);
    }

    if(bron == VLO)
    {
        clkStartOscillator(VLO);
    }

    BCSCTL1 &= ~(0x3<<4);
    BCSCTL1 |= (clkGetDivision(deler)<<4);
}

void clkConfigureMCLK(Oscillator bron, uint8_t deler)
{
    //standaard DCOCLK
    uint8_t sel=0;

    if(bron == LFXT1)
    {
        sel=3;
        clkStartOscillator(LFXT1);
    }

    if(bron == VLO)
    {
        sel=3;
        clkStartOscillator(VLO);
    }

    BCSCTL2 &= 0x0F;
    BCSCTL2 |= (sel<<6 | clkGetDivision(deler)<<4);
}

void clkStartOscillator(Oscillator bron)
{
    if(bron==VLO)
    {
        BCSCTL3 = LFXT1S_2;
    }

    if(bron==LFXT1)
    {
        BCSCTL3 = LFXT1S_0 | XCAP_1;
        IFG1 &= ~OFIFG;

        //wachten voor stabiliteit
        __delay_cycles(800);
        while(IFG1 & OFIFG)
        {
            IFG1 &= ~OFIFG;
            __delay_cycles(800);
        }
    }
}

uint8_t clkGetDivision(uint8_t deler)
{
    switch(deler)
    {
        case 1: deler=0; break;
        case 2: deler=1; break;
        case 4: deler=2; break;
        case 8: deler=3; break;
    }

    return deler;
}


#pragma vector = WDT_VECTOR
__interrupt void WDT_ISR(void)
{
  if(wdcnt) wdcnt--;
  else {
      systick++;
      wdcnt = 250;
  }
  __bic_SR_register_on_exit(LPM0_bits);     // Exit LPM0
}

