/*
 * ina219.h
 *
 *  Created on: 28 jun. 2019
 *      Author: klind
 */
#include <stdint.h>

#ifndef LIB_INA219_H_
#define LIB_INA219_H_

// Shift the 7 bit address one bit and add a 1 to init a read command
#define READBYTE(ADDR) (ADDR<<1)|1
// Shift the 7 bit address one bit and leave the 0 to init a write command
#define WRITEBYTE(ADDR) (ADDR<<1)

// Pointer addresses
#define CNF_POINTER 0x00
#define VSH_POINTER 0x01
#define VLT_POINTER 0x02
#define PWR_POINTER 0x03
#define CUR_POINTER 0x04
#define CAL_POINTER 0x05

// CONFIG BITS
#define RST_SHIFT 15

#define BUS_RNG_16 (0<<13)
#define BUS_RNG_32 (1<<13)

#define PGA_DIV_1 (0<<11)
#define PGA_DIV_2 (1<<11)
#define PGA_DIV_4 (2<<11)
#define PGA_DIV_8 (3<<11)

#define SADC_SHIFT 3
#define BADC_SHIFT 7
#define SADC SADC_SHIFT
#define BADC BADC_SHIFT

#define ADC_9BIT_1SMPL(SHFT) (0<<SHFT)
#define ADC_10BIT_1SMPL(SHFT) (1<<SHFT)
#define ADC_11BIT_1SMPL(SHFT) (2<<SHFT)
#define ADC_12BIT_1SMPL(SHFT) (3<<SHFT)
#define ADC_12BIT_2SMPL(SHFT) (9<<SHFT)
#define ADC_12BIT_4SMPL(SHFT) (10<<SHFT)
#define ADC_12BIT_4SMPL(SHFT) (10<<SHFT)
#define ADC_12BIT_8SMPL(SHFT) (11<<SHFT)
#define ADC_12BIT_16SMPL(SHFT) (12<<SHFT)
#define ADC_12BIT_32SMPL(SHFT) (13<<SHFT)
#define ADC_12BIT_64SMPL(SHFT) (14<<SHFT)
#define ADC_12BIT_128SMPL(SHFT) (15<<SHFT)

#define MODE_PWR_DWN (0<<0)
#define MODE_SHUNT_TRIG (1<<0)
#define MODE_BUS_TRIG (2<<0)
#define MODE_SHUNTBUS_TRIG (3<<0)
#define MODE_ADC_OFF (4<<0)
#define MODE_SHUNT_CONT (5<<0)
#define MODE_BUS_CONT (6<<0)
#define MODE_SHUNTBUS_CONT (7<<0)

// Conversion factors
#define BUS_GAIN 4
#define SHUNT_GAIN 1
#define CURRENT_GAIN


typedef struct {
    uint8_t addr;           // 7-bit Address of the device
    uint16_t power;         // Measured power <Read-only>
    uint16_t voltage;       // Measured bus voltage <Read-only>
    uint16_t current;       // Measured current <Read-only>
    int initiated;          // Device initiate flag <Read-only>
    uint16_t bus_range;     //// Bus voltage range options:
                            // [] BUS_RNG_16
                            // [] BUS_RNG_32
    uint16_t pga_gain;      //// Shunt voltage PGA Gain options:
                            // [] PGA_DIV1
                            // [] PGA_DIV2
                            // [] PGA_DIV4
                            // [] PGA_DIV8
    uint16_t sadc_mode;     // Shunt ADC mode
    uint16_t badc_mode;     // Bus ADZ mode
    uint16_t mode;          // ADC trigger Mode
    uint16_t calib;         // Current calibration value (see Datasheet INA219)
} ina219;

int ina219Init(ina219 *device);
void ina219WriteConfig(ina219 *device, int reset);
void ina219ReadAll(ina219 *device);
void ina219ReadVoltage(ina219 *device);
void ina219ReadPower(ina219 *device);
void ina219ReadCurrent(ina219 *device);
void ina219GetPower(ina219 *device);
void ina219GetVoltate(ina219 *device);
void ina219GetCurrent(ina219 *device);
void ina219SetAddress(ina219 *device, uint8_t addr);
void ina219SetBusRange(ina219 *device,uint16_t range);
void ina219SetPGAGain(ina219 *device, uint16_t gain);
void ina219WriteCalib(ina219 *device);

#endif /* LIB_INA219_H_ */
