#include <stdint.h>

#define PWM_PERIOD 319
#define MAX_DUTY 0.9

extern const uint16_t pwmLimit;

void initPWM(void);
void setPWM(uint16_t value);
