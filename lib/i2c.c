#include <msp430.h>
#include <stdint.h>
#include <stdbool.h>

#include "i2c.h"
#include "gpio.h"

i2cMode i2cStatus = MASTER;

void i2cStart()
{
    //start
    UCB0CTL1 |= UCTXSTT;
}

void i2cStop()
{
    //stop
    UCB0CTL1 |= UCTXSTP;
    while(UCB0CTL1 & UCTXSTP); //wachten op een acknowledge
}

void i2cStopPrepare(){
    //stop
    UCB0CTL1 |= UCTXSTP;
}

void i2cInitialize(i2cMode mode)
{
    //zet de i2c module in reset modus
    setBits(UCB0CTL1, UCSWRST);

    //set to synchronous I2C mode
    setBits(UCB0CTL0,  UCMODE_3 | UCSYNC);

    //i2c functie selecteren pinnetjes
    gpioSetFunction(1, 6, SECONDARY);
    gpioSetFunction(1, 7, SECONDARY);

    //Bij 16mhz levert dit 800khz.
    //Sneller dan gespecificeerd maar
    //lijkt zonder problemen te werken
    i2cSetClock(SMCLK, 20);

    i2cSetMode(mode);

    //haal uit reset modus
    clearBits(UCB0CTL1, UCSWRST);


}

void i2cSetClock(clockSources source, uint16_t divider)
{
    //klok selecteren
    clearBits(UCB0CTL1, (0x3<<6));
    setBits(UCB0CTL1, (source<<6));

    //klokdeler selecteren voor frequentie
    UCB0BR0 = (divider & 0x00FF);
    UCB0BR1 = (divider>>8);
}

void i2cSetMode(i2cMode mode)
{
    i2cStatus = mode;

    switch(mode)
    {
        case MASTER:
            setBits(UCB0CTL0,   UCMST);
            break;

        case SLAVE:
        default:
            clearBits(UCB0CTL0,   UCMST);
    }
}

void i2cSetAddress(uint8_t address)
{
    UCB0I2CSA = address;
}

void i2cSendBytes(uint16_t length, const uint8_t bytes[])
{
    //versturen, dus write bit moet aan
    UCB0CTL1 |= UCTR;

    //start conditie op de bus zetten
    i2cStart();

    uint8_t byteCounter;
    for(byteCounter=0; byteCounter<length; byteCounter++)
    {
       i2cSendByte(bytes[byteCounter]);
    }

    i2cStop();
}

void i2cSendByte(const uint8_t data)
{
    UCB0TXBUF = data;
    while(!(IFG2 & UCB0TXIFG)); //wachten op ifg
}

uint8_t  i2cReadByte(void){
        //Uitlezen, dus write bit moet uit
        UCB0CTL1 &= ~UCTR;

        //start conditie op de bus zetten
        i2cStart();
        // Wachten tot de start bit weer 0 is
        while(UCB0CTL1 & UCTXSTT);
        // Stop de bus en stuur NACK en STOP signaal
        i2cStop();
        // Wacht op de data
        while(!(IFG2 & UCB0RXIFG));
        uint8_t data = UCB0RXBUF;
        return data;
}

void i2cReadBytes(uint16_t length, uint8_t data[]){
        //Uitlezen, dus write bit moet uit
        UCB0CTL1 &= ~UCTR;

        //start conditie op de bus zetten
        i2cStart();
        // Blijf ontvangen tot de een na laatste byte
        uint16_t byteCount;
        for(byteCount = 0; byteCount < (length - 1); byteCount++){
            // Wacht op de data
            while(!(IFG2 & UCB0RXIFG));
            // Sla de data op in de array
            data[byteCount] = UCB0RXBUF;
        }
        // Stop de bus en stuur NACK en STOP signaal
        i2cStop();
        // Wacht op de laatste byte
        while(!(IFG2 & UCB0RXIFG));
        data[length - 1] = UCB0RXBUF;
}

// De volgende functie voert een sequentie van read en write operaties uit.
// Dit kan handig zijn om bijvoorbeeld een bepaald register van een i2c slaaf uit te lezen.
// Het adres van de slaaf moet al van tevoren ingesteld zijn.
// de volgende commando bytes zijn mogelijk in de sequence array
// -- I2C_READ
// Elke andere byte wordt naar de bus geschreven

void i2cSendSequence(uint16_t length, const uint16_t sequence[], uint8_t data[]){
    bool i2cReading = false;
    uint16_t seqCount;
    uint16_t dataCount;

    if(sequence[0] == I2C_READ){
        // Start de bus en zet write bit uit
        UCB0CTL1 &= ~UCTR;
        i2cStart();
        i2cReading = true;
    }else{
        // Start de bus en zet write bit aan
        UCB0CTL1 |= UCTR;
        i2cStart();
        i2cReading = false;
    }

    for(seqCount = 0; seqCount < (length); seqCount++){

        // Wachten tot de start bit 0 is
        while(UCB0CTL1 & UCTXSTT);

        if(seqCount == length - 1){
            i2cStopPrepare();
        }else{
            // Bepaal wat de volgende handeling is
            if(sequence[seqCount + 1] == I2C_READ){
                if(!i2cReading){
                    // Reset de bus en zet write bit uit
                    UCB0CTL1 &= ~UCTR;
                    i2cStart();
                    i2cReading = true;
                }
            }else{
                // Geen commando dus schrijven naar de bus
                if(i2cReading){
                    // Reset de bus en zet write bit aan
                    UCB0CTL1 |= UCTR;
                    i2cStart();
                    i2cReading = false;
                }
            }
        }

        if(sequence[seqCount] == I2C_READ){
            // Wacht op de byte
            while(!(IFG2 & UCB0RXIFG));
            data[dataCount] = UCB0RXBUF;
            dataCount++;
        }else{
            UCB0TXBUF = (char)sequence[seqCount];
            // Wacht tot het verzonden is
            while(!(IFG2 & UCB0TXIFG));
        }
    }
}
