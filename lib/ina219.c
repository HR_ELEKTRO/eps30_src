/*
 * ina219.c
 *
 *  Created on: 28 jun. 2019
 *      Author: klind
 */
#include "ina219.h"
#include "i2c.h"

int ina219Init(ina219 *device){
    if(device->addr && !device->initiated){
        // Do init
        ina219WriteConfig(device, 0);
        ina219WriteCalib(device);
        device->initiated = 1;
        return 1;
    }

    return 0;
}

void ina219WriteConfig(ina219 *device, int reset){
    // Split de data
    uint16_t data = (reset<<RST_SHIFT)|(device->bus_range)|(device->pga_gain)|(device->badc_mode)|(device->sadc_mode)|(device->mode);
    uint8_t data1 = (data>>8);
    uint8_t data2 = data&0x0F;
    // Stuur de bytes
    uint8_t send_bytes[] = {CNF_POINTER, data1, data2};
    uint16_t result;
    i2cSetAddress(device->addr);
    i2cSendBytes(3, send_bytes);
}

void ina219WriteCalib(ina219 *device){
    // Prepare the data
    uint8_t data1 = ((device->calib)>>8);
    uint8_t data2 = ((device->calib)&0x0F);
    // Prepare the full send sequence
    uint8_t send_bytes[] = {CAL_POINTER, data1, data2};
    // Send the sequence
    i2cSetAddress(device->addr);
    i2cSendBytes(3, send_bytes);
}

void ina219ReadAll(ina219 *device){
    // See if the device is initiated
    if(device->initiated){
        ina219ReadPower(device);
        ina219ReadVoltage(device);
        ina219ReadCurrent(device);
    }
}

void ina219ReadPower(ina219 *device){
    // See if the device is initiated
    if(device->initiated){
        i2cSetAddress(device->addr);
        uint16_t read_power_sequence[] = {PWR_POINTER, I2C_READ, I2C_READ};
        uint8_t result[2];
        i2cSendSequence(3, read_power_sequence, &result);
        device->power = (uint16_t)(result[0]<<8) | (result[1]);
    }
}

void ina219ReadVoltage(ina219 *device){
    // See if the device is initiated
    if(device->initiated){
        i2cSetAddress(device->addr);
        uint16_t read_voltage_sequence[] = {VLT_POINTER, I2C_READ, I2C_READ};
        uint8_t result[2];
        i2cSendSequence(3, read_voltage_sequence, &result);
        uint16_t full_register = (uint16_t)(result[0]<<8) | (result[1]);
        device->voltage = (full_register >> 3)*BUS_GAIN;
    }
}

void ina219ReadCurrent(ina219 *device){
    // See if the device is initiated
    if(device->initiated){
        i2cSetAddress(device->addr);
        uint16_t read_current_sequence[] = {CUR_POINTER, I2C_READ, I2C_READ};
        uint8_t result[2];
        i2cSendSequence(3, read_current_sequence, &result);
        device->current = (uint16_t)(result[0]<<8) | (result[1]);
    }
}

uint16_t getCurrent(ina219 *device){
    return device->current;
}

uint16_t getVoltage(ina219 *device){
    return device->voltage;
}

uint16_t getPower(ina219 *device){
    return device->power;
}

void setAddress(ina219 *device, uint8_t addr){
    device->addr = addr;
}
