#include <msp430.h>
#include <stdint.h>
#include "gpio.h"


//Deze functie is leesbaar maar heeft dubbele code
void gpioSetDirection(uint8_t port, uint8_t pin, gpioDirection direction)
{
    if(port==2)
    {
        if(direction==OUTPUT)
        {
            setBit(P2DIR, pin); //1 -> output pin
        }
        else
        {
            clearBit(P2DIR, pin); //0 -> input pin
        }
    }
    else
    {
        if(direction==OUTPUT)
        {
            setBit(P1DIR, pin);
        }
        else
        {
            clearBit(P1DIR, pin);
        }
    }
}

//deze functie is iets algemener maar gebruikt 'pointers'
void gpioSet(uint8_t port, uint8_t pin, gpioValue value)
{
    //Koppeling register van port 1 of 2 aan variabele
    //onderstaande code is gelijk aan
    //'volatile unsigned char * PORTOUT = (port==2? &P2DIR : &P1DIR);'
    volatile unsigned char * PORTOUT;

    if(port==2)
    {
        PORTOUT = &P2DIR;
    }
    else
    {
        PORTOUT = &P1DIR;
    }


    if(value==HIGH)
    {
        setBit(*PORTOUT, pin);
    }
    else
    {
        clearBit(*PORTOUT, pin);
    }

}

void gpioSetFunction(uint8_t port, uint8_t pin, gpioFunction function)
{
    //Koppeling register van port 1 of 2 aan variabele
    volatile unsigned char * PSEL   = (port==2? &P2SEL : &P1SEL);
    volatile unsigned char * PSEL2  = (port==2? &P2SEL2 : &P1SEL2);

    switch(function)
    {
        case PRIMARY:
            setBit  (*PSEL,     pin);
            clearBit(*PSEL2,    pin);
            break;

        case SECONDARY:
            setBit  (*PSEL,     pin);
            setBit  (*PSEL2,    pin);
            break;

        case CAPACITIVESENSING:
            clearBit(*PSEL,     pin);
            setBit  (*PSEL2,    pin);
            break;

        case DIGITAL:
        default:
            clearBit(*PSEL,     pin);
            clearBit(*PSEL2,    pin);
    }

}
