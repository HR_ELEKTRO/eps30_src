################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lib/clock.c \
../lib/gpio.c \
../lib/i2c.c \
../lib/ina219.c \
../lib/pwm.c 

C_DEPS += \
./lib/clock.d \
./lib/gpio.d \
./lib/i2c.d \
./lib/ina219.d \
./lib/pwm.d 

OBJS += \
./lib/clock.obj \
./lib/gpio.obj \
./lib/i2c.obj \
./lib/ina219.obj \
./lib/pwm.obj 

OBJS__QUOTED += \
"lib\clock.obj" \
"lib\gpio.obj" \
"lib\i2c.obj" \
"lib\ina219.obj" \
"lib\pwm.obj" 

C_DEPS__QUOTED += \
"lib\clock.d" \
"lib\gpio.d" \
"lib\i2c.d" \
"lib\ina219.d" \
"lib\pwm.d" 

C_SRCS__QUOTED += \
"../lib/clock.c" \
"../lib/gpio.c" \
"../lib/i2c.c" \
"../lib/ina219.c" \
"../lib/pwm.c" 


