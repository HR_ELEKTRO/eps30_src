#include <msp430.h> 
#include <stdint.h>
#include "lib/gpio.h"
#include "lib/clock.h"
#include "lib/pwm.h"
#include "lib/i2c.h"
#include "lib/ina219.h"

/**
 * EPS10 firmware for Buck converter experiment board
 *
 *
 */

// INA Devices:
ina219 inaInput = {
                .addr = 0x40,
                .bus_range = BUS_RNG_32,
                .pga_gain = PGA_DIV_2,
                .badc_mode = ADC_12BIT_1SMPL(BADC),
                .sadc_mode = ADC_12BIT_1SMPL(SADC),
                .mode = MODE_SHUNTBUS_CONT,
                .calib = 8192
            };

ina219 inaOutput= {
                   .addr = 0x41,
                   .bus_range = BUS_RNG_32,
                   .pga_gain = PGA_DIV_2,
                   .badc_mode = ADC_12BIT_1SMPL(BADC),
                   .sadc_mode = ADC_12BIT_1SMPL(SADC),
                   .mode = MODE_SHUNTBUS_CONT,
                   .calib = 8192
               };

void mainInit(void);
void setLED(void);
void clearLED(void);

static uint32_t lastVoltageRead = 0;
static uint16_t power[2];
static uint16_t pwm[2];
const uint16_t step = 1;


int main(void)
{

    mainInit();

    // Initialize INA devices
    ina219Init(&inaInput);
    ina219Init(&inaOutput);

    while(1)
    {
        __bis_SR_register(LPM0_bits + GIE);     // LPM0, WDT_ISR will force exit

        // Every 10ms read the measurements
        if(lastVoltageRead + 15 < systick){
            // Toggle led for timing
            setLED();

            ina219ReadAll(&inaInput);
            ina219ReadAll(&inaOutput);
            lastVoltageRead = systick;

            // Shift the last power value and get the power measurement
            power[1] = power[0];
            power[0] = inaInput.power;

            // PERTURB AND OBSERVE START
            if(power[0] > 10 && inaInput.initiated){
                // If the power increased
                if(power[0] > power[1]){
                    // and we where increasing the dutycycle
                    if(pwm[0] > pwm[1]){
                        // increase the duty again
                        pwm[1] = pwm[0];
                        uint16_t new_pwm = pwm[0] + step;
                        if(new_pwm < pwmLimit){
                            pwm[0] = new_pwm;
                        }
                    }else{
                        // else ( if we where decreasing the duty)
                        // decrease the duty again
                        pwm[1] = pwm[0];
                        uint16_t new_pwm = pwm[0] - step;
                        if(new_pwm > 0){
                            pwm[0] = new_pwm;
                        }
                    }
                }else{ // power[0] < power[1]
                    // If the power is decreasing
                    if(pwm[0] > pwm[1]){
                        // Go the other direction
                        pwm[1] = pwm[0];
                        uint16_t new_pwm = pwm[0] - step;
                        if(new_pwm > 0){
                            pwm[0] = new_pwm;
                        }
                    }else{
                        // else ( if we where decreasing the duty)
                        // Go the other direction
                        pwm[1] = pwm[0];
                        uint16_t new_pwm = pwm[0] + step;
                        if(new_pwm < pwmLimit){
                            pwm[0] = new_pwm;
                        }
                    }
                } // power[0] > power[1]
                setPWM(pwm[0]);
            }// if power > 10
            // PERTURB AND OBSERVE END
            // Toggle led for timing
            clearLED();

        }// Systick timer
    }
}

void mainInit(void){
    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */

    initPWM();
    i2cInitialize(MASTER);
    // Set LED pin to output and low
    setBit(P1DIR, 5); // P1.5 as output (LED)
    clearBit(P1OUT, 5);
}

void setLED(void){
    setBit(P1OUT, 5);
}

void clearLED(void){
    clearBit(P1OUT, 5);
}
